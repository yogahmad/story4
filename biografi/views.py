from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'home.html')

def contact(request):
    return render(request, 'contact.html')

def welcome(request):
    return render(request, 'welcome.html')

def profile(request):
    return render(request, 'profile.html')

def experiences(request):
    return render(request, 'experiences.html')